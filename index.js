const express  = require('express')
const app = express()
const logroute = require('./routes/loginRoute')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
dotenv.config()

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())
app.use(express.static('Login'))
app.set('view engine','ejs');
app.set('views','./Login')



//use the route middleware
app.use('/',logroute)

mongoose.connect(process.env.MONGO_URI,
	{useNewUrlParser: true,useCreateIndex:true,useUnifiedTopology:true},
	()=>{
		console.log('connected to DB')
	})


app.listen(3000,()=>{console.log('server running on port 3000');})