const express = require('express')
const router = express.Router()
const schema = require('../model/schema')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var localStorage = require('localStorage')
const multer = require('multer')
const path = require('path')
const galleryschema = require('../model/gallery')
const gallery = mongoose.model('gallery')

var Storage = multer.diskStorage({
	destination:(req,file,cb)=>{
	    cb(null,'./uploads')},
	filename:(req,file,cb)=>{
	    cb(null,file.fieldname+'-'+Date.now()+path.extname(file.originalname));

	}
});

var upload = multer({
	storage:Storage
}).single('avatar');



//upload image
router.get('/upload',(req,res,next)=>{
res.render('gallery',{msg :" "});
});

router.post('/profile', upload, function (req, res, next) {
	// req.file is the `avatar` file
	// req.body will hold the text fields, if there were any
	res.send("uploaded file" )
	res.send(req.file )
  })

router.post('/upload',(req,res,next)=>{
	// res.render('gallery',{msg :" "});
	console.log(req.file)
    console.log(req.files)

upload(req,res,(err)=>{
	if(err){res.send('err is'+err)}
	else{
		var img = req.file.filename
		var newImage = new gallery({image:img})
		newImage.save((err,data)=>{
			if(err) throw err;
	 		res.send('image uploaded')
		})

	}
})


	   
   });

const usermodel = mongoose.model('basic')
//finding and storing in users variable
var users = usermodel.find({})


//middleware to check if user has loggedin
function checkLoginUser(req,res,next){
	var userToken = localStorage.getItem('userToken')
		try {
	  var decoded = jwt.verify(userToken, 'logintoken');
	} catch(err) {
     res.redirect('/');
	}
	next();
}


//to check during signup if email already exists
  function existEmail (req,res,next){
	// var email = req.body.email;
	var checkemail = usermodel.findOne({email:req.body.email})
	checkemail.exec((err,data)=>{
		if(err) throw err;
		if(data){
		console.log(data)

		return res.render('signup',{msg:'Email already exists'})
	    console.log('working')
		}
		next();
	});
}


//get the  member login page
router.get('/',(req,res,next)=>{
 res.render('index',{msg :" "});
	
})


//post route for login page
router.post('/',(req,res,next)=>{
	var password = req.body.password;
	var Email = req.body.email;
 	var checkemail = usermodel.findOne({email:req.body.email})
 	checkemail.exec((err,user)=>{
 		if(err) throw err;
 		var getuserId = user._id;
 		var getpassword =user.password;
 		console.log(getpassword)
 		console.log(bcrypt.compareSync(req.body.password, getpassword))

 		if (bcrypt.compareSync(req.body.password, getpassword)){
 			console.log('if part');
 			var token =jwt.sign({userID :getuserId},'logintoken');
 			console.log(JSON.stringify(token))
 			localStorage.setItem('userToken', JSON.stringify(token));
 			localStorage.setItem('loginMail',Email)
 			 res.redirect('/dashboard');
 			 

 		}
 		else{
 			console.log('else part')
 			res.render('index',{msg :" email or password is wrong"});
 			
 		}

 	})
	
})

//get the admin dashboard
router.post('/dashboard',checkLoginUser,(req,res,next)=>{
 res.render('dashboard')
	
})

//get the member signup page
router.get('/signup',(req,res,next)=>{
 res.render('signup',{msg :' '})
	
})



//signup new member
router.post('/signup',existEmail,(req,res,next)=>{
// const newName = req.body.fullname;
// const newPhone = req.body.phone;
// const newYear = req.body.year;
// const newEmail = req.body.email;
 if(req.body.password != req.body.cpassword){
 	res.render('signup',{msg :" passwords do not match "})
 }
else{
	const password = bcrypt.hashSync(req.body.password,13);
	 var newuser = new usermodel(
	 	{   fullname:req.body.fullname,
	 		phone:req.body.phone,
	 		year:req.body.year,
	 		email:req.body.email,
	 		password:password 
	 	 })

		newuser.save((err,data)=>{
			if(err) throw err;
			res.render('signup',{msg :"You're Added"})
			console.log('saved')
		})
 
}

    
    
})


module.exports = router